function DataSeries(name)
{
    this.name = name;
    this.data = [];
}

DataSeries.prototype.getData = function()
{
    return this.data;
}

DataSeries.prototype.addDataPoint = function(label, value)
{
    this.data.push(new DataPoint(label, value));
}

DataSeries.prototype.getName = function()
{
    return this.name;
}

function DataPoint(label, value)
{
    this.label = label;
    this.value = value;
}

DataPoint.prototype.getLabel = function()
{
    return this.label;
}

DataPoint.prototype.getValue = function()
{
    return this.value;
}

function initializeData()
{
    var series1 = new DataSeries("bar");
    
    var series2 = new DataSeries("line");
   
   // var series3 = new DataSeries("Easthampton");
    
    data2.push(series1);
    data2.push(series2);
    
}

var data2 = [];
initializeData();

//console.info(data[0].data[0].value);