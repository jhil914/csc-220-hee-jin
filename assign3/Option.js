/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Option(x,y,width,height,name){//position, width,height, name
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
    this.name = name;
    this.isMouseOver = false;
    this.font = 'italic 8pt sans-serif';
    this.bg_color = 'white';
    this.isSelect = false;
}
//check if mouse is inside the option
Option.prototype.checkMouse = function(pos){
    var check = false;
    if (pos.x >= this.x && pos.y >= this.y &&
            pos.x <= this.x + this.width &&
            pos.y <= this.y + this.height) {
       // this.isMouseOver = true;
        check = true;
     //   console.info(this.y);
    }//else{
     //   this.isMouseOver = false;
    //    check = false;
  //  }
    
    return check;
}
//set feature of individual options
Option.prototype.set_feature = function(font,bg_color,txt_color,txtAlign){//font,backgorund color, 
    this.font = font;
    this.color = txt_color;
    this.bg_color = bg_color;
    this.txtAlign = txtAlign;
}
