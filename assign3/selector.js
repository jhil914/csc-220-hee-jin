/* 
 *Selector class that holds options
 */

function Selector(x,y,width,height,type){
    this.x=x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.options = [];
    this.isMouseOver = false;
    this.isExpanded = false;
    this.type =type;
    // adds initial selector as an option into the options array
    this.add(new Option(this.x,this.y,this.width,this.height,type));
}
 
Selector.prototype = new Option();
//sets options features 
Selector.prototype.set_opt_features = function(font, bg_color, txt_color, textAlign){
    for (var i = 1; i < this.options.length; i++) {
        this.options[i].set_feature(font, bg_color, txt_color, textAlign);
    }
}
//initialize the selector style
Selector.prototype.InitializeSelector = function(g){
    g.fillStyle = this.bg_color;
    g.beginPath();
    g.rect(this.x, this.y, this.width, this.height);
   g.strokeStyle = "#333";
    g.fill();
    g.stroke();
    g.fillStyle = 'black';
    g.textAlign = 'center';
    g.font = 'italic 8pt sans-serif';
    g.fillText(this.type,this.x+this.width/2,this.y+this.height/2);
    
}
//adds in the options
Selector.prototype.add = function(option){
    this.options.push(option);
}
//draws options
Selector.prototype.draw = function(g){
    
    
    for (var i =1; i<this.options.length;i++){
        var center_x = this.options[i].x+this.options[i].width/2;
        var center_y = this.options[i].y+this.options[i].height/2;
        g.fillStyle = this.options[i].bg_color;
        g.font = this.options[i].font;
        g.textAlign = this.options[i].txtAlign;

        g.beginPath();
        g.rect(this.options[i].x, this.options[i].y, this.options[i].width, this.options[i].height);
        g.fill();
        g.fillStyle = 'white';
        g.stroke();
        g.fillText(this.options[i].name,center_x,center_y);
    }
}
// check if the selector has expanded
Selector.prototype.check_expanded = function(pos){
    var total_height =this.height*this.options.length;
    if (pos.x >= this.x && pos.y >= this.y &&
            pos.x <= this.x + this.width &&
            pos.y <= this.y + total_height) {
        this.isExpanded = true;
    }else{
        this.isExpanded = false;
    }
    return this.isExpanded;
}


