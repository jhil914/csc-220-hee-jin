/**
 * Producing Selectors with options that when mouse is hovered,
 *  a dropdown menu is shown with options
 * CSC 220
 * Hee Jin
 * 
 * - problems: feel like there should be another main class that is
 * on a higher hierarchy than this file. drawing each selector demo
 * in separate canvases seem inefficient
 * 
 */
var g = null;
// initialize the canvas environment
function initialize() {
    // demo for location selector
    var demo = new SelectorDemo("location", data);
    demo.initialize(document.getElementById("canvas"));
    // demo for graph selector
    var demo2 = new SelectorDemo("graph", data2);
    demo2.initialize(document.getElementById("canvas2"));
}
// selectorDemo takes in the name and the data as input
function SelectorDemo(name, data) {
    //basic attributes
    var x = 0;
    var y = 0;
    var width = 80;
    var height = 30;
    this.name = name;
    this.selector = new Selector(x, y, width, height, this.name);
    var font = 'italic 8pt sans-serif';
    var textAlign = "center";
    var bg_color = 'blue';
    var txt_color = 'white';
    this.DATA = data;

    //makes options for the selector and add it
    for (var i = 0; i < this.DATA.length; i++) {
        var option = new Option(x, y + height, width, height, this.DATA[i].name);
        //  option.set_feature(font,bg_color,txt_color,textAlign);
        this.selector.add(option);
        y = y + height;
    }
    
    this.selector.set_opt_features(font, bg_color, txt_color, textAlign);

}
SelectorDemo.prototype = new GameEngine();
//clears the dropdown options
SelectorDemo.prototype.clear = function (g) {
    g.fillStyle = "white";

    var x = this.selector.x - 10;
    var y = this.selector.y + 10;
    var width = this.selector.width + 20;
    var height = this.selector.height * this.selector.options.length + 10;
    g.fillRect(x, y, width, height);
}
// draws the objects depending on its conditions
SelectorDemo.prototype.draw = function (g) {

    if (this.selector.isMouseOver) {
        this.selector.InitializeSelector(g);
        this.selector.draw(g);
    } else {
        this.clear(g);
        this.selector.InitializeSelector(g);
    }
}
// mouseclick info processed only when the mouse has been hovered
//over the initial selector and the drop down is created
SelectorDemo.prototype.onMouseClick = function (pos) {
    for (var i = 0; i < this.selector.options.length; i++) {
        var opt = this.selector.options[i];
        //  console.info(opt.isMouseOver);
        if (opt.isMouseOver && this.selector.isExpanded && this.selector.isMouseOver) {
            opt.isSelect = true;
            console.info(this.selector.options[i].name);
        } else {
            opt.isSelect = false;
        }
    }
}
// checks if the mouse is over the initial selector state
SelectorDemo.prototype.onMouseMove = function (pos) {

    if (this.selector.check_expanded(pos)) {
        if (this.selector.options[0].checkMouse(pos)) {
            this.selector.isMouseOver = true;

        }
    } else {
        // console.info("out");
        this.selector.isMouseOver = false;
    }
    if (this.selector.isExpanded && this.selector.isMouseOver) {
        for (var i = 1; i < this.selector.options.length; i++) {
            //  console.info(i);
            if (this.selector.options[i].checkMouse(pos)) {
                this.selector.options[i].isMouseOver = true;
                this.selector.options[i].bg_color = 'red';
                //     console.info(i);
            } else {
                this.selector.options[i].isMouseOver = false;
                this.selector.options[i].bg_color = 'blue';
            }
        }
    }
}
window.onload = initialize;