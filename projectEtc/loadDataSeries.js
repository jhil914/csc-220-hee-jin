/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function DataSeries(csvfile) {
    this.rawdata = csvfile;
    this.name = null;
    this.values = [];
    this.data = [];

}
DataSeries.prototype = new Bubble();

DataSeries.prototype.processData = function () {
    this.rawdata = this.rawdata.split("\n");
    var firstLine = this.rawdata[0].split(",");

    for (var k = 0; k < firstLine.length; k++) {
        var tmpArray = [];
        for (var i = 1; i < this.rawdata.length; i++) {
            var eachLine = this.rawdata[i].split(",");
            tmpArray.push(eachLine[k]);
        }
        this.data.push(tmpArray);
    }
}

DataSeries.prototype.setName = function (name) {
    this.name = name;
}

DataSeries.prototype.getValues = function () {
    return this.values;
}

DataSeries.prototype.setValueLabels = function (valuesArray) {
    this.valuesLabel = valuesArray;
    for (var i = 0; i < this.valuesLabel.length; i++) {
        this.values.push(new DataColumn(this.valuesLabel[i], this.data[i]));
    }
    this.initializeSize();
}
DataSeries.prototype.initializeSize = function () {
    this.radius = this.values[1].getValue()[0] / 1.1;
    this.width = this.radius;
    this.height = this.radius;

}

function DataColumn(label, values) {
    this.label = label;
    this.value = values;
}

DataColumn.prototype = new DataSeries();

DataColumn.prototype.getLabel = function () {
    return this.label;
}

DataColumn.prototype.getValue = function () {

    return this.value;
}

DataColumn.prototype.setValue = function (values) {
    this.value = values;
}