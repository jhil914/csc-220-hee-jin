/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function TimeBar() {
    this.ID = "TimeBar";
}   

TimeBar.prototype = new ClickableElements();

TimeBar.prototype.initialSetting = function (type, pos, width, height) {
    this.name = type;
    this.pos = pos;
    this.width = width;
    this.height = height;
    this.timeChip = new TimeChip(this.pos);
}

TimeBar.prototype.draw = function (g) {
    g.fillStyle = "blue";
    g.strokeStyle = "red";
    g.lineWidth = 20;
    g.beginPath();
    g.moveTo(this.pos.getX(), this.pos.getY());
    g.lineTo(this.pos.getX() + this.width, this.pos.getY());
    this.timeChip.drawPath(g);
    g.closePath();
    g.fill();
    g.stroke();
    g.fillStyle = this.textColor;
    g.font = "30px Aeial";
    g.fillText(this.name, this.pos.getX(), this.pos.getY());
}

function TimeChip(pos) {
    this.pos = pos;
    this.name = "sdf";
    this.width = 30;
    this.height = 30;
}

TimeChip.prototype.drawPath = function (g) {
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
}

