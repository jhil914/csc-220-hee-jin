/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Bubble() {
    this.fillColor = "orange";
    this.strokeCol = "black";
    this.strokeThick = 5;
    this.displayText = "";
    this.displayFont = "30px Aeial";
    this.ID = "Bubble";
    this.dataRowIndex = 0;
}

Bubble.prototype = new DraggableElements();

Bubble.prototype.setDisplayName = function (displayName, font, color) {
    this.displayText = displayName;
    this.font = font;
    this.textColor = color;
}

Bubble.prototype.setFillColor = function (color) {
    this.fillColor = color;
}

Bubble.prototype.setStrokeColor = function (color) {
    this.strokeCol = color;
}

Bubble.prototype.setStrokeThickness = function (thickness) {
    this.strokeThick = thickness;
}

Bubble.prototype.changeSize = function () {
    if (this.dataRowIndex < this.getValues()[1].getValue().length) {
        this.dataRowIndex = this.dataRowIndex + 1;

    }
    else { 
        console.info("end of data");
    }
    this.radius = this.getValues()[1].getValue()[this.dataRowIndex];
    this.width = this.radius / 1.1;
    this.height = this.radius / 1.1;
}

Bubble.prototype.draw = function (g) {
    g.fillStyle = this.fillColor;
    g.strokeStyle = this.strokeCol;
    g.lineWidth = this.strokeThick;
    g.beginPath();
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
    g.closePath();
    g.fill();
    g.stroke();
    g.fillStyle = this.textColor;
    g.font = this.font;
    g.fillText(this.displayText, this.pos.getX(), this.pos.getY());
    g.font = "10px Aeial";
    g.fillText(this.radius, this.pos.getX() + this.width / 2, this.pos.getY() + this.height / 2);
}

