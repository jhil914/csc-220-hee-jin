function Matrix(m, n) {
    this.m = m;
    this.n = n;
    this.data = [];
    for (var i = 0; i < n; i++) {
        var row = [];
        for (var j = 0; j < m; j++) {
            row.push(0);
        }
        this.data.push(row);
    }
}

Matrix.getRotation = function(a) {
    var rotation = new Matrix(3, 3);
    rotation.set(0, 0, Math.cos(a));
    rotation.set(1, 0, -Math.sin(a));
    rotation.set(0, 1, Math.sin(a));
    rotation.set(1, 1, Math.cos(a));
    rotation.set(2, 2, 1);
    return rotation;
}

Matrix.getRotationAt = function(a, x, y) {
    return Matrix.getTranslate(x, y).multiply(
            Matrix.getRotation(a).multiply (
                Matrix.getTranslate(-x, -y)));
}

Matrix.getScale = function(sx, sy) {
    var scale = new Matrix(3, 3);
    scale.set(0, 0, sx);
    scale.set(1, 1, sy);
    scale.set(2, 2, 1);
    return scale;
}

Matrix.getScaleAt = function(sx, sy, x, y) {
    return Matrix.getTranslate(x, y).multiply(
            Matrix.getScale(sx, sy).multiply (
                Matrix.getTranslate(-x, -y)));
}

Matrix.getTranslate = function(x, y) {
    var translate = new Matrix(3, 3);
    translate.set(0, 0, 1);
    translate.set(1, 1, 1);
    translate.set(2, 2, 1);
    translate.set(2, 0, x);
    translate.set(2, 1, y);
    return translate;
}

Matrix.fromPoint = function(p) {
    var point = new Matrix(1, 3);
    point.set(0, 0, p.x);
    point.set(0, 1, p.y);
    point.set(0, 2, 1);
    return point;
}

Matrix.prototype.set = function(m, n, value) {
    this.data[n][m] = value;
}

Matrix.prototype.get = function(m, n) {
    return this.data[n][m];
}

Matrix.prototype.getM = function() {
    return this.m;
}

Matrix.prototype.getN = function() {
    return this.n;
}

Matrix.prototype.multiply = function(matrix) {
    var isParamPoint = false;
    if (matrix instanceof Point) {
        isParamPoint = true;
        matrix = Matrix.fromPoint(matrix);
    }
    if (this.getM() != matrix.getN()) {
        throw "Matrix can't be multiplied";
    } else {
        var result = new Matrix(matrix.getM(), this.getN());
        for (var m = 0; m < result.getM(); m++) {
            for (var n = 0; n < result.getN(); n++) {
                var dotProduct = 0;
                for (var i = 0; i < this.getM(); i++) {
                    dotProduct += this.get(i, n) * matrix.get(m, i);
                }
                result.set(m, n, dotProduct);
            }
        }
        if (isParamPoint) {
            return new Point(result.get(0, 0), result.get(0, 1));
        } else {
            return result;
        }
    }
}

Matrix.prototype.toString = function() {
    var maxDigits = 0;
    for (var n = 0; n < this.data.length; n++) {
        for (var m = 0; m < this.data[n].length; m++) {
            var digits = Math.round(this.data[n][m]).toString().length;
            maxDigits = Math.max(maxDigits, digits);
        }
        returnString += outputLine + "\n";
    }
    
    var returnString = "";
    var brackets = " _";
    while (brackets.length < this.m * (maxDigits + 2) + (this.m - 1) + 2) {
        brackets += " ";    
    }
    brackets += "_ \n";
    returnString += brackets;
    for (var n = 0; n < this.data.length; n++) {
        var outputLine = n < this.data.length - 1 ? "| " : "|_";
        for (var m = 0; m < this.data[n].length; m++) {
            var alignedValue = (Math.round(this.data[n][m] * 10) / 10)
                    .toString();
            if (!alignedValue.includes(".")) {
                alignedValue += ".0";
            }
            while (alignedValue.length < maxDigits + 2) {
                alignedValue = " " + alignedValue;
            }
            outputLine += alignedValue + (m < this.m - 1 ? " " : "");
        }
        returnString += outputLine + 
                (n < this.data.length - 1 ? " |\n" : "_|\n");
    }
    return returnString;
}

Matrix.prototype.setGraphicsTransformation = function(g) {
    
}