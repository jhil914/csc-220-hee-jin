/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function setupTestScenario() {
    var gameLoop = new CustomGameLoop();
    gameLoop.initialize(document.getElementById("canvas"));
    gameLoop.setCanvasSize(750, 841);

    var backgroundImage = new Background();
    backgroundImage.loadUrl("sdf.png");
    backgroundImage.setPosition(new Point(0, 0));
    backgroundImage.setWidth(750);
    backgroundImage.setHeight(841);
    gameLoop.addElement(backgroundImage);
    
    var Timebar = new TimeBar();
    Timebar.initialSetting("ByHour(Click me!)",new Point (300,300),50,10);
    Timebar.setPosition(new Point(300,300));
    Timebar.setWidth(100);
    Timebar.setHeight(30);
    gameLoop.addElement(Timebar);
     
    var CampusCenter = new DataSeries(request.responseText);
    CampusCenter.processData();
    CampusCenter.setName("campuscenter");
    CampusCenter.setValueLabels(["Timestamp", "electricity", "water"]);
    CampusCenter.setPosition(new Point(100, 100));
    CampusCenter.setFillColor("#2BBDBD");
    CampusCenter.setStrokeColor("orange");
    CampusCenter.setStrokeThickness(5);
    CampusCenter.setDisplayName("Campus Center","30px Aeial","black");
    gameLoop.addElement(CampusCenter);
    
}

function initialize() {
    setupTestScenario();
}

window.onload = initialize;