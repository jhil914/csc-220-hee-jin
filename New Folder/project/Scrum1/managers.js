/* 
The element manager manages the visual elements in the canvas
and keeps track of its ID
 */

function ElementManager() {
    this.elements = [];
    this.element_id = 999999;
    this.timebars = [];
    this.bubbles = [];
}

ElementManager.prototype.add = function (element) {
    if(element.ID==="TimeBar"){
        this.timebars.push(element);
    }
    else if(element.ID==="Bubble"){
        this.bubbles.push(element);
    }
    this.elements.push(element);
}
ElementManager.prototype.reactToTime = function(){
    for(var i=0;i<this.bubbles.length;i++){
        this.bubbles[i].changeSize();
          
    }
}

ElementManager.prototype.getElement = function (pointer_pos) {
    for (var i in this.elements) {
        var w = this.elements[i].width;
        var h = this.elements[i].height;
        var pos = this.elements[i].pos;
        if (pointer_pos.getX() > pos.getX() && pointer_pos.getX() < pos.getX() + w
                && pointer_pos.getY() > pos.getY() && pointer_pos.getY() < pos.getY() + h) {
            this.element_id = i;
    
        }
    }
    this.elements[this.element_id].id = this.element_id;
    return this.elements[this.element_id];
}
ElementManager.prototype.drawElements = function (g) {
    for (var id in this.elements) {
        this.elements[id].draw(g);
    }
}
      
//pointer manager takes an elementmanager as a parameter
//to access the elements stored in the elementmanager
//it keeps track of all the pointers and call specific functions
function PointerManager(EM) {
    this.pointers = [];
    this.EM = EM;
    this.element_click = new ClickableElements();
}

PointerManager.prototype.onPointerEnter = function (id, pos) {
    this.addPointer(id, pos);
}

PointerManager.prototype.onPointerMove = function (id, pos) {
 
    this.movePointer(id, pos);
}
PointerManager.prototype.associatePointerDrag = function (elementManager) {

}

PointerManager.prototype.onPointerActivate = function (id, pos) {
    this.pointers[id].activate(this.EM.getElement(pos),pos);
  if(this.EM.getElement(pos).ID==="TimeBar"){   
      this.EM.reactToTime();
  }
    this.pointers[id].init_drag( pos);
}

PointerManager.prototype.onPointerDeactivate = function (id, pos) {
    this.pointers[id].deactivate();
}

PointerManager.prototype.onPointerLeave = function (id, pos) {
    this.removePointer(id, pos);
}

PointerManager.prototype.hasPointer = function (id) {
    return typeof this.pointers[id] != 'undefined';
}

PointerManager.prototype.addPointer = function (id, initialPosition) {
    this.pointers[id] = new Pointer(id, initialPosition);
}

PointerManager.prototype.movePointer = function (id, pos) {
    //   console.info("movepointer");
    this.pointers[id].move(pos);
    //  this.pointers[id].follow()
}

PointerManager.prototype.removePointer = function (id, pos) {
    delete this.pointers[id];
}

PointerManager.prototype.drawPointerDebugOverlay = function (g) {
    for (var id in this.pointers) {
        this.pointers[id].drawDebugOverlay(g);
    }
}
// </editor-fold>

// <editor-fold desc="InputManager">
//each individial pointer class associated with functions
function Pointer(id, initialPosition) {
    this.id = id;
    this.position = initialPosition.clone();
    this.isActive = false;
    this.pointerOnObj = false;
   
}

Pointer.prototype.move = function (pos) {
  
    this.position.setX(pos.getX());
    this.position.setY(pos.getY());
    this.follow( pos);
}
Pointer.prototype.init_drag = function (pos) {

    if (typeof this.clickablelement.hitTest !== 'undefined') {
        if (this.pointerOnObj) {
            this.clickablelement.start_drag(pos);
        }
    }
    return this.pointerOnObj;
}
Pointer.prototype.follow = function ( pos) {
    if (this.pointerOnObj) {
        this.clickablelement.move_drag(pos); 
    }
}
Pointer.prototype.end_follow = function (pos) {
    if (!this.pointerOnObj) {
        this.clickablelement.end_drag(pos);
        
    }
}
Pointer.prototype.getPosition = function (pos) {
    return this.position.clone();
}

Pointer.prototype.getIsActive = function () {
    // this.draggablelement.start_drag();
    return this.isActive;
}

Pointer.prototype.drawDebugOverlay = function (g) {
    g.strokeStyle = "black";
    g.fillStyle = "black";
    g.font = "10px Arial"
    g.lineWidth = this.getIsActive() ? 3 : 1;
    g.globalAlpha = this.getIsActive() ? 1 : 0.5;
    var position = this.getPosition();
    g.beginPath();
    g.rect(position.getX() - 20, position.getY() - 20, 40, 40);
    g.stroke();
    g.fillText(this.id, position.getX() - 20, position.getY() - 20 - 3);
    g.globalAlpha = 1.0;
}

Pointer.prototype.activate = function (clickablelement, pos) {
    this.clickablelement = clickablelement;
       
    if (typeof clickablelement.hitTest !== 'undefined') {
       if(this.clickablelement.ID==="TimeBar"){
           console.info("it's a timebar")     
       }     
    }
    this.isActive = true;
}
   
Pointer.prototype.deactivate = function () {
    this.pointerOnObj = false;
    this.isActive = false;
}
