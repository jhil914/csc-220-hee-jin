/* 
 * Shows a bubble representation of the input building data
 * and size changes depending on the timebar
 */


function setupTestScenario() {
    var gameLoop = new CustomGameLoop();
    gameLoop.initialize(document.getElementById("canvas"));
    gameLoop.setCanvasSize(750, 841);

    var backgroundImage = new Background();
    backgroundImage.loadUrl("sdf.png");
    backgroundImage.setPosition(new Point(0, 0));
    backgroundImage.setWidth(750);
    backgroundImage.setHeight(841);
    gameLoop.addElement(backgroundImage);

    var timeBar = new TimeBar();
    timeBar.initialSetting("ByHour(Click me!)", new Point(250, 500), 50, 10);
    timeBar.setPosition(new Point(250, 500));
    timeBar.setWidth(30);
    timeBar.setHeight(30);
    gameLoop.addElement(timeBar);

    var waterFilter = new FilterButton();
    waterFilter.setPosition(new Point(600, 500));
    // Filtermenu.addFilter("electricity");
    waterFilter.addFilter("water");
    waterFilter.setWidth(120);
    waterFilter.setHeight(30);
    waterFilter.setFillColor("white");
    waterFilter.setStrokeColor("orange");
    waterFilter.setStrokeThickness(5);
    gameLoop.addElement(waterFilter);

    var electricityFilter = new FilterButton();
    electricityFilter.setPosition(new Point(600, 450));
    // Filtermenu.addFilter("electricity");
    electricityFilter.addFilter("electricity");
    electricityFilter.setWidth(120);
    electricityFilter.setHeight(30);
    electricityFilter.setFillColor("white");
    electricityFilter.setStrokeColor("orange");
    electricityFilter.setStrokeThickness(5);
    gameLoop.addElement(electricityFilter);

    var campusCenterData = JSON.parse(JSON.stringify(campuscenter));

    var campusCenter = new Bubble(campusCenterData);
    campusCenter.setPosition(new Point(100, 100));
    campusCenter.initialize();
    campusCenter.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    campusCenter.setStrokeColor("black");
    campusCenter.setStrokeThickness(5);
    campusCenter.setDisplayName("Campus Center", "30px Aeial", "black");
    gameLoop.addElement(campusCenter);

}

function initialize() {
    setupTestScenario();
}

window.onload = initialize;