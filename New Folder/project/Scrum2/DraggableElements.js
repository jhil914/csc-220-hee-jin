/* 
 * dragging movement that can be implemented to movable objects
 */

  
function DraggableElements() {
    this.isDragging = false;
    this.posStack = [];
   
}   
DraggableElements.prototype = new HitTestableElement();
DraggableElements.prototype.startDrag = function (pos) {
    this.initX = this.pos.getX();
    this.initY = this.pos.getY();

    this.initPointX = pos.getX();
    this.initPointY = pos.getY();
                                               
    this.isDragging = true;
    this.posStack.push(pos);
}
DraggableElements.prototype.moveDrag = function (pos) {
this.posHistory(pos);
   // this.pos.setY(this.initY + pos.getY() - this.initPointY);
   
    this.pos.setX(this.initX + pos.getX() - this.initPointX);
    
}
DraggableElements.prototype.posHistory = function(pos){
    if(this.posStack.length>=3){
        this.posStack.shift();
    }else{
    this.posStack.push(pos);
    }
}
DraggableElements.prototype.getDirection = function(pos){
   // console.info(this.posStack[0]);
    if(this.posStack[0].getX()-pos.getX()<0){
        this.direction = "right";
    }
    else{
        this.direction = "left";
    }
    return this.direction;
}
DraggableElements.prototype.checkDrag = function(){
    return this.isDragging;
}
