/* 
 *class that manages all the visual obj in the canvas
 */

function Visual(width, height) {
    this.width = width;
    this.height = height;
}

Visual.prototype.setFillColor = function (fillColor) {
    this.fillColor = fillColor;
}

Visual.prototype.setStrokeColor = function (color) {
    this.strokeCol = color;
}

Visual.prototype.setStrokeThickness = function (thickness) {
    this.strokeThick = thickness;
}

Visual.prototype.setWidth = function (w) {
    this.width = w;
}
Visual.prototype.setHeight = function (h) {
    this.height = h;
}
Visual.prototype.setPosition = function (pos) {
    this.pos = pos;
}
Visual.prototype.draw = function (g) {
    this.draw(g);
}
// tests whether the position of the pointer is in the boundary
//of a specific obj
function HitTestableElement() {
    this.hit = false;    
    
}
HitTestableElement.prototype = new Visual();

HitTestableElement.prototype.hitTest = function (pt) {
    
    if (pt.getX() < this.pos.getX() + this.width && pt.getX() > this.pos.getX()
            && pt.getY() > this.pos.getY() && pt.getY() < this.pos.getY() + this.height) {
        this.hit = true;
    } else {     
        this.hit = false;
    }
    return this.hit;
}