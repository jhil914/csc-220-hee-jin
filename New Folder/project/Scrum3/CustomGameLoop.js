/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function CustomGameLoop() {
    this.elementManager = new ElementManager();
   // this.timeBarsManager = new TimeBarManager();
    
    this.pointerManager = new PointerManager(this.elementManager);
}
CustomGameLoop.prototype = new GameLoop();

CustomGameLoop.prototype.initialize = function (canvas) {
    GameLoop.prototype.initialize.call(this, canvas);

}
CustomGameLoop.prototype.setCanvasSize = function (w, h) {
    this.canvas.height = h;
    this.canvas.width = w;
}
CustomGameLoop.prototype.addElement = function (element) {
    this.elementManager.add(element);

}

CustomGameLoop.prototype.draw = function (g) {
    g.fillStyle = "lightgray";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.elementManager.drawElements(g);
    this.pointerManager.drawPointerDebugOverlay(g);
}
CustomGameLoop.prototype.onPointerEnter = function (id, pos) {

    this.pointerManager.onPointerEnter(id, pos);
}
CustomGameLoop.prototype.onPointerMove = function (id, pos) {

    this.pointerManager.onPointerMove(id, pos);
}
CustomGameLoop.prototype.onPointerActivate = function (id, pos) {

    this.pointerManager.onPointerActivate(id, pos);
}
CustomGameLoop.prototype.onPointerDeactivate = function (id, pos) {

    this.pointerManager.onPointerDeactivate(id, pos);
}
CustomGameLoop.prototype.onPointerLeave = function (id, pos) {

    this.pointerManager.onPointerLeave(id, pos);
}
 