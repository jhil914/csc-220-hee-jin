/* 
 * Bubble shape representation of the data input
 */

function Bubble(dataObj) {
    this.fillColor = "orange";
    this.strokeCol = "black";
    this.strokeThick = 5;
    this.displayText = "";
    this.displayFont = "30px Aeial";
    this.ID = "Bubble";
    this.dataRowIndex = 0;
    this.dataObj = dataObj;
    this.data = "electricity";   
    this.opacity = 0.6;
    this.popUp = new PopUp();
}

Bubble.prototype = new ClickableElements();    

Bubble.prototype.initialize = function(){
     this.radius = this.dataObj[0][this.data] / 1.1;
    this.width = this.radius;
    this.height = this.radius;
    this.popUp.setPosition(new Point(this.pos.getX()+this.radius+10,
                                                 this.pos.getY()));
}

Bubble.prototype.setDisplayName = function (displayName, font, color) {
    this.displayText = displayName;
    this.font = font;
    this.textColor = color;
}

Bubble.prototype.setFillColors = function (colorJSON) {
    this.fillColorJSON = colorJSON;
}

Bubble.prototype.changeSize = function (index) {
   
    if (index < this.dataObj.length && index >=0) {
        this.dataRowIndex = index;
    }
    else { 
        console.info("end of data");
    }
    
    this.radius = this.dataObj[this.dataRowIndex][this.data];
    this.width = this.radius / 1.1;
    this.height = this.radius / 1.1;
    this.popUp.setPosition(new Point(this.pos.getX()+this.radius+10,
                                       this.pos.getY()));
}

Bubble.prototype.draw = function (g) {
    g.globalAlpha = this.opacity;
    g.fillStyle = this.fillColorJSON[this.data];
    g.strokeStyle = this.strokeCol;
    g.lineWidth = this.strokeThick;
    this.drawBubble(g);
    g.globalAlpha = 1;
    this.drawText(g);
    this.popUp.setDisplayText("This is a popup");
    this.popUp.draw(g);
  
}

Bubble.prototype.drawBubble = function(g){
      g.beginPath();  
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
    g.closePath(); 
    g.fill();    
    g.stroke();
}

Bubble.prototype.drawText = function(g){
     g.fillStyle = this.textColor;
    g.font = this.font;
    g.fillText(this.displayText, this.pos.getX(), this.pos.getY()-10);
    g.font = "15px Aeial";
    g.fillText(this.radius, this.pos.getX() + this.width+10, 
    this.pos.getY() + this.height / 2);
}

