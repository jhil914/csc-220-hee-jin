//creates background
function Background() {

    this.img = new Image();
    
}
Background.prototype = new Visual();

Background.prototype.loadUrl = function (url) {
    this.url = url;
    this.img.src = this.url;
}
Background.prototype.draw = function (g) {

    g.drawImage(this.img, this.pos.getX(), this.pos.getY(), this.width, this.height);

}

