/* 
 *TimeBar that controls the size of the building bubbles
 */

function TimeBar() {
    this.ID = "TimeBar";
}

TimeBar.prototype = new DraggableElements();
//initial setting of the Timebar
TimeBar.prototype.initialSetting = function (type, pos, width, height) {
    this.name = type;
    this.pos = pos;
    this.width = width;
    this.height = height;
    this.timeLine = new TimeLine(this.pos);
}

TimeBar.prototype.reactToTime = function (bubbles) {
    for (var i = 0; i < bubbles.length; i++) {
        var increment = this.timeLine.width / bubbles[i].dataObj.length;
        var index = Math.floor((this.pos.getX() + this.width / 2 - this.timeLine.pos.getX()) / increment)

        bubbles[i].changeSize(index);

    }
}

TimeBar.prototype.draw = function (g) {
    g.fillStyle = "blue";
    g.strokeStyle = "red";
    g.lineWidth = 10;
    this.timeLine.drawPath(g);
    g.stroke();
    g.closePath();
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
    g.fill();
    g.fillStyle = this.textColor;
    g.font = "30px Aeial";
    g.fillText(this.name, this.pos.getX(), this.pos.getY());
}

TimeBar.prototype.isOnTimeLine = function(pos){
    return (pos.getX()>=this.timeLine.pos.getX()
                && pos.getX()<=this.timeLine.pos.getX()+
                this.timeLine.width);
}

//Timechip on the barLine that can be dragged around in the future
//for now, only clicking event is implemented
function TimeLine(pos) {
    this.pos = pos;
    this.name = "sdf";
    this.width = 300;
    this.height = 30;
  
}

TimeLine.prototype = new Visual();

TimeLine.prototype.drawPath = function (g) {
    g.beginPath();
    g.moveTo(this.pos.getX(), this.pos.getY() + this.height / 2);
    g.lineTo(this.pos.getX() + this.width, this.pos.getY() + this.height / 2);
    
}

