/* 
 * clicking movement that can be implemented to clickable objects
 */


function ClickableElements() {
    this.isClickedNum = 0;
   
}
ClickableElements.prototype = new HitTestableElement();

ClickableElements.prototype.popUpActivate = function(){
    if(this.isClickedNum ===1){
        this.popUp.isActive=false;
        this.isClickedNum = 0;
    }else{
        this.popUp.isActive = true;
        this.isClickedNum = this.isClickedNum +1;
    }
    
}


