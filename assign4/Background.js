//creates background
function Background() {

    this.img = new Image();
}
Background.prototype = new Visual();

Background.prototype.loadUrl = function (url) {
    this.url = url;
    this.img.src = this.url;
}
Background.prototype.draw = function (g) {

    g.drawImage(this.img, this.pos.getX(), this.pos.getY(), this.width, this.height);

}
// tests whether the position of the pointer is in the boundary
//of a specific obj
function HitTestableElement() {
    this.hit = false;
}
HitTestableElement.prototype = new Visual();
HitTestableElement.prototype.hitTest = function (pt) {
    if (pt.getX() < this.pos.getX() + this.width && pt.getX() > this.pos.getX()
            && pt.getY() > this.pos.getY() && pt.getY() < this.pos.getY() + this.height) {
        this.hit = true;
    } else {
        this.hit = false;
    }
    return this.hit;
}
