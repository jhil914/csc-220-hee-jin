/* 
 * dragging movement that can be implemented to movable objects
 */


function DraggableElements() {
    this.is_dragging = false;
}
DraggableElements.prototype = new HitTestableElement();
DraggableElements.prototype.start_drag = function (pos) {
    this.init_x = this.pos.getX();
    this.init_y = this.pos.getY();

    this.init_point_x = pos.getX();
    this.init_point_y = pos.getY();

    this.is_dragging = true;
}
DraggableElements.prototype.move_drag = function (pos) {

    this.pos.setY(this.init_y + pos.getY() - this.init_point_y); 
    this.pos.setX(this.init_x + pos.getX() - this.init_point_x);

}
DraggableElements.prototype.check_drag = function(){
    return this.is_dragging;
}
DraggableElements.prototype.end_drag = function () {
   

}