/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Triangle() {

}
Triangle.prototype = new Shape();
Triangle.prototype.drawPath = function (g) {

    g.moveTo(this.pos.getX(), this.pos.getY() + this.height);
    g.lineTo(this.pos.getX() + this.width / 2, this.pos.getY());
    g.lineTo(this.pos.getX() + this.width, this.pos.getY() + this.height);

}
function Circle() {

}
Circle.prototype = new Shape();
Circle.prototype.drawPath = function (g) {
    //   g.beginPath();
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
}
function Rectangle() {

}
Rectangle.prototype = new Shape();
Rectangle.prototype.drawPath = function (g) {
    g.rect(this.pos.getX(), this.pos.getY(), this.width, this.height);
}