/* 
 *class that manages all the visual obj in the canvas
 */

function Visual(width, height) {
    this.width = width;
    this.height = height;
  //  this.pos = new Point();
}

Visual.prototype.setWidth = function (w) {
    this.width = w;
}
Visual.prototype.setHeight = function (h) {
    this.height = h;
}
Visual.prototype.setPosition = function (pos) {
    this.pos = pos;
}
Visual.prototype.draw = function (g) {
    this.draw(g);
}
