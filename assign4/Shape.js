/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Shape() {

}
Shape.prototype = new DraggableElements();

Shape.prototype.setFillColor = function (color) {
    this.fillColor = color;
}
Shape.prototype.setStrokeColor = function (color) {
    this.strokeCol = color;
}
Shape.prototype.setStrokeThickness = function (thickness) {
    this.strokeThick = thickness;
}
Shape.prototype.draw = function (g) {
    g.fillStyle = this.fillColor;
    g.strokeStyle = this.strokeCol;
    g.lineWidth = this.strokeThick;
    g.beginPath();
    this.drawPath(g);
    g.closePath();
    g.fill();
    g.stroke();
}

