/**
 * 
 * @param {array} dataObj
 * @returns {TimeBar}
 */
function TimeBar(dataObj) {
    this.ID = "TimeBar";
    this.dataObj = dataObj;
    Visual.call(this.width, this.height);
}
/**
 * 
 * @type DraggableElements
 */
TimeBar.prototype = new DraggableElements();


/**
 * 
 * @param {String} type
 * @param {Point} pos
 * @param {int} width
 * @param {int} height
 * @returns {undefined}
 */
TimeBar.prototype.initialSetting = function (type, pos, width, height) {

    this.name = type;
    this.setPosition(pos);
    this.width = width;
    this.height = height;
    this.timeLine = new TimeLine();
    this.timeLine.setPosition(this.pos);
    this.DisplayCurrentTime = new TimeDisplay(this.dataObj);

}
/**
 * 
 * @param {Point} pos
 * @returns {undefined}
 */
TimeBar.prototype.moveDrag = function (pos) {
    this.pos.setX(this.initX + pos.getX() - this.initPointX);
}
/**
 * 
 * @param {Bubble[]} bubbles
 * @returns {undefined}
 */
TimeBar.prototype.reactToTime = function (bubbles) {
    for (var i = 0; i < bubbles.length; i++) {
        var increment = this.timeLine.width / bubbles[i].dataObj.length;
        var index = Math.floor((this.pos.getX() + this.width / 2 - this.timeLine.pos.getX()) / increment)

        bubbles[i].changeSize(index);
        this.DisplayCurrentTime.setTimeIndex(index);
    }
}
/**
 * 
 * @returns {undefined}
 */
TimeBar.prototype.scaleYPosition = function () {
    this.pos.y = (window.innerHeight * this.origPosY / origBackgroundHeight);
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
TimeBar.prototype.draw = function (g) {
    this.scaleYPosition();
    g.fillStyle = "blue";
    g.strokeStyle = "red";
    g.lineWidth = 10;
    this.timeLine.drawPath(g);
    g.stroke();
    g.closePath();
    TOOLS.drawEllipse(g, this.pos.getX(), this.pos.getY(), this.width, this.height);
    g.fill();
    g.fillStyle = this.textColor;
    g.font = "30px Aeial";
    g.fillText(this.name, this.pos.getX(), this.pos.getY());
    this.DisplayCurrentTime.drawPath(g);

}
/**
 * 
 * @param {Point} pos
 * @returns {Boolean}
 */
TimeBar.prototype.isOnTimeLine = function (pos) {
    return (pos.getX() >= this.timeLine.pos.getX()
            && pos.getX() <= this.timeLine.pos.getX() +
            this.timeLine.width);
}

//Timechip on the barLine that can be dragged around in the future
//for now, only clicking event is implemented
/**
 * 
 * @returns {TimeLine}
 */
function TimeLine() {
    this.name = "sdf";
    this.width = window.innerHeight / 1.2;
    this.height = 30;

}

TimeLine.prototype = new Visual();
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
TimeLine.prototype.drawPath = function (g) {
    this.width = window.innerHeight / 1.2;
    g.beginPath();
    this.scalePosition();
    g.moveTo(this.pos.getX(), this.pos.getY() + this.height / 2);
    g.lineTo(this.pos.getX() + this.width, this.pos.getY() +
            this.height / 2);

}
/**
 * 
 * @param {Array} dataObj
 * @returns {TimeDisplay}
 */
function TimeDisplay(dataObj) {
    this.dataObj = dataObj;
    this.currentTime = this.dataObj[0]["timeStamp"];
    this.fillStyle = "white";
    this.strokeStyle = "black";
    this.strokeThick = 3;
    this.displayFont = "30px Aeial";
    this.width = 60;
    this.height = 30;
    this.pos = new Point(0, window.innerHeight - this.height);
}

TimeDisplay.prototype = new Visual();
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
TimeDisplay.prototype.drawPath = function (g) {
    this.pos = new Point(0, window.innerHeight - this.height);
    g.fill();
    g.stroke();
    g.fillStyle = this.strokeStyle;
    g.font = this.displayFont;
    g.fillText(this.currentTime, this.pos.getX(), this.pos.getY() +
            this.height / 2);
}
/**
 * 
 * @param {int} timeIndex
 * @returns {undefined}
 */
TimeDisplay.prototype.setTimeIndex = function (timeIndex) {

    if (timeIndex < this.dataObj.length) {
        this.currentTime = this.dataObj[timeIndex]["timeStamp"];
    } else {
        this.currentTime = null;
    }

}
/**
 * 
 * @returns {String} currentTime
 */
TimeDisplay.prototype.getTime = function () {
    return this.currentTime;
}
