/**
 * //class for background
 * @returns {Background}
 */
function Background() {

    this.img = new Image();
    this.ID = "Background";
}
Background.prototype = new Visual();
/**
 * 
 * @param {string} url
 * @returns {undefined}
 */
Background.prototype.loadUrl = function (url) {
    this.url = url;
    this.img.src = this.url;
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
Background.prototype.draw = function (g) {

    g.drawImage(this.img, this.pos.getX(), this.pos.getY(), window.innerHeight / 1.2, window.innerHeight);

}

