/**
 * class that manages all the visual obj in the canvas
 * @param {int} width
 * @param {int} height
 * @returns {Visual}
 */
function Visual(width, height) {
    this.width = width;
    this.height = height;
}
/**
 * 
 * @param {Color} fillColor
 * @returns {undefined}
 */
Visual.prototype.setFillColor = function (fillColor) {
    this.fillColor = fillColor;
}
/**
 * 
 * @param {Color} color
 * @returns {undefined}
 */
Visual.prototype.setStrokeColor = function (color) {
    this.strokeCol = color;
}
/**
 * 
 * @param {int} thickness
 * @returns {undefined}
 */
Visual.prototype.setStrokeThickness = function (thickness) {
    this.strokeThick = thickness;
}
/**
 * 
 * @param {int} w
 * @returns {undefined}
 */
Visual.prototype.setWidth = function (w) {
    this.width = w;
}
/**
 * 
 * @param {int} h
 * @returns {undefined}
 */
Visual.prototype.setHeight = function (h) {
    this.height = h;
}
/**
 * 
 * @param {Point} pos
 * @returns {undefined}
 */
Visual.prototype.setPosition = function (pos) {
    this.origPosX = pos.getX();
    this.origPosY = pos.getY();
    this.pos = pos;
}
/**
 * scales the pos depending on the innerwidth and 
 * background size
 * @returns {undefined}
 */
Visual.prototype.scalePosition = function () {

    this.pos.x = ((window.innerHeight * this.origPosX / origBackgroundWidth) / 1.2);
    this.pos.y = (window.innerHeight * this.origPosY / origBackgroundHeight);
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
Visual.prototype.draw = function (g) {
    this.draw(g);
}
/**
 * tests whether the position of the pointer is in the boundary
* of a specific obj
 * @returns {HitTestableElement}
 */
function HitTestableElement() {
    this.hit = false;
}

HitTestableElement.prototype = new Visual();
/**
 * HIitests given pos 
 * @param {Point} pt
 * @returns {Boolean}
 */
HitTestableElement.prototype.hitTest = function (pt) {

    if (pt.getX() < this.pos.getX() + this.width && pt.getX() > 
            this.pos.getX()&& pt.getY() > this.pos.getY() 
            && pt.getY() < this.pos.getY() + this.height) {
        this.hit = true;
    } else {
        this.hit = false;
    }
    return this.hit;
}