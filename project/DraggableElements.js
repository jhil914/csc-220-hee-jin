
 /**
  * dragging movement that can be implemented to movable objects
  * @returns {DraggableElements}
  */
function DraggableElements() {
    this.isDragging = false;
    this.posStack = [];
   
}   

DraggableElements.prototype = new HitTestableElement();
/**
 * 
 * @param {Point} pos
 * @returns {undefined}
 */
DraggableElements.prototype.startDrag = function (pos) {
    this.initX = this.pos.getX();
    this.initY = this.pos.getY();

    this.initPointX = pos.getX();
    this.initPointY = pos.getY();
                                               
    this.isDragging = true;
    this.posStack.push(pos);
}
/**
 * 
 * @param {Point} pos
 * @returns {undefined}
 */
DraggableElements.prototype.moveDrag = function (pos) {

    this.pos.setY(this.initY + pos.getY() - this.initPointY);
    this.pos.setX(this.initX + pos.getX() - this.initPointX);
    
}
