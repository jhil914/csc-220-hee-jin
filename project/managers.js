/**
 *  The element manager manages the visual elements in the canvas
 and keeps track of its ID
 * @returns {ElementManager}
 */
function ElementManager() {
    this.elements = [];
    this.elementID = 999999;
    this.timebars = [];
    this.bubbles = [];
    this.filters = [];
}
/**
 * categorize the added implements
 * @param {Visual} element
 * @returns {undefined}
 */
ElementManager.prototype.add = function (element) {

    if (element.ID === "TimeBar") {
        this.timebars.push(element);
    }
    else if (element.ID === "Bubble") {
        this.bubbles.push(element);
    }
    else if (element.ID === "Filter") {
        this.filters.push(element);
    }
    this.elements.push(element);
}
/**
 * react function based on its ID
 * @param {Visual} element
 * @returns {undefined}
 */
ElementManager.prototype.react = function (element) {
    if (typeof element !== 'undefined') {
        if (element.ID === "TimeBar") {
            element.reactToTime(this.bubbles);
        }
        else if (element.ID === "Filter") {
            element.reactToFilter(this.bubbles);
            this.refreshTimebar();
        }
    }
}
/**
 * refreshes the timebar when different menus are cicked
 * @returns {undefined}
 */
ElementManager.prototype.refreshTimebar = function () {
    for (var i = 0; i < this.timebars.length; i++) {
        this.timebars[i].reactToTime(this.bubbles);
    }
}
/**
 * gets the object under the mouse
 * @param {Point} pointerPos
 * @returns {Array}
 */
ElementManager.prototype.getElement = function (pointerPos) {

    for (var i = 1; i < this.elements.length; i++) {

        var w = this.elements[i].width;
        var h = this.elements[i].height;
        var pos = this.elements[i].pos;
        if (this.elements[i].hitTest(pointerPos)) {
            this.elementID = i;
            console.info(i);
            this.elements[this.elementID].id = this.elementID;
            return this.elements[this.elementID];
        }
    }
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
ElementManager.prototype.drawElements = function (g) {

    for (var id in this.elements) {

        this.elements[id].draw(g);
    }
}
/**
 * pointer manager takes an elementmanager as a parameter
 to access the elements stored in the elementmanager
 it keeps track of all the pointers and call specific functions
 * @param {ElementManager} elementManager
 * @returns {PointerManager}
 */
function PointerManager(elementManager) {
    this.pointers = [];
    this.elementManager = elementManager;

}

PointerManager.prototype.onPointerEnter = function (id, pos) {
    this.addPointer(id, pos);
}

PointerManager.prototype.onPointerMove = function (id, pos) {

    this.movePointer(id, pos);
}
PointerManager.prototype.associatePointerDrag = function (elementManager) {

}

PointerManager.prototype.onPointerActivate = function (id, pos) {

    this.pointers[id].activate(this.elementManager.getElement(pos), pos);
    this.elementManager.react(this.elementManager.getElement(pos));
}

PointerManager.prototype.onPointerDeactivate = function (id, pos) {
    this.pointers[id].deactivate();
}

PointerManager.prototype.onPointerLeave = function (id, pos) {
    this.removePointer(id, pos);
}

PointerManager.prototype.hasPointer = function (id) {
    return typeof this.pointers[id] != 'undefined';
}

PointerManager.prototype.addPointer = function (id, initialPosition) {
    this.pointers[id] = new Pointer(id, initialPosition);
}

PointerManager.prototype.movePointer = function (id, pos) {
    this.dragPointer(id, pos);
}

PointerManager.prototype.dragPointer = function (id, pos) {
    if (this.pointers[id].drag(pos)) {
        this.elementManager.react(this.elementManager.getElement(pos));
    }
}

PointerManager.prototype.removePointer = function (id, pos) {
    delete this.pointers[id];
}

PointerManager.prototype.drawPointerDebugOverlay = function (g) {
    for (var id in this.pointers) {
        this.pointers[id].drawDebugOverlay(g);
    }
}
// </editor-fold>

// <editor-fold desc="InputManager">
//each individial pointer class associated with functions
function Pointer(id, initialPosition) {
    this.id = id;
    this.position = initialPosition.clone();
    this.isActive = false;
    this.pointerOnObj = false;
}

Pointer.prototype.drag = function (pos) {
    this.position.setX(pos.getX());
    this.position.setY(pos.getY());
    if (this.pointerOnObj) {
        if (this.draggablelement.isOnTimeLine(pos)) {
            this.draggablelement.moveDrag(pos);
            return true;
        } else {
            return false;
        }
    }
}
/**
 * start Dragging
 * @param {Point} pos
 * @returns {Boolean}
 */
Pointer.prototype.initDrag = function (pos) {

    if (typeof this.draggablelement.hitTest !== 'undefined') {
        if (this.pointerOnObj) {
            this.draggablelement.startDrag(pos);
        }
    }
    return this.pointerOnObj;
}
/**
 * get the pos
 * @param {Point} pos
 * @returns {Point}
 */
Pointer.prototype.getPosition = function (pos) {
    return this.position.clone();
}
/**
 * 
 * @returns {Boolean}
 */
Pointer.prototype.getIsActive = function () {
    // this.draggablelement.start_drag();
    return this.isActive;
}

Pointer.prototype.drawDebugOverlay = function (g) {
    g.strokeStyle = "black";
    g.fillStyle = "black";
    g.font = "10px Arial"
    g.lineWidth = this.getIsActive() ? 3 : 1;
    g.globalAlpha = this.getIsActive() ? 1 : 0.5;
    var position = this.getPosition();
    g.beginPath();
    g.rect(position.getX() - 20, position.getY() - 20, 40, 40);
    g.stroke();
    g.fillText(this.id, position.getX() - 20, position.getY() - 20 - 3);
    g.globalAlpha = 1.0;
}
/**
 * 
 * @param {Visual} element
 * @param {Point} pos
 * @returns {undefined}
 */
Pointer.prototype.activate = function (element, pos) {
    if (typeof element !== 'undefined') {
        if (element.hitTest(pos)) {
            if (element.ID === "Bubble") {
                this.clickablelement = element;
                this.clickablelement.activateEvent();
            } else if (element.ID === "TimeBar") {
                this.draggablelement = element;
                this.pointerOnObj = true;
                this.initDrag(pos);
            }
        }
    }
    this.isActive = true;
}
/**
 * 
 * @returns {undefined}
 */
Pointer.prototype.deactivate = function () {
    this.pointerOnObj = false;
    this.isActive = false;

}

