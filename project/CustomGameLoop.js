/**
 * 
 * Extended gameLoop
 * @returns {CustomGameLoop}
 */
function CustomGameLoop() {
    this.elementManager = new ElementManager();
    this.pointerManager = new PointerManager(this.elementManager);
}
CustomGameLoop.prototype = new GameLoop();
/**
 * fill window when the window is resized
 * @returns {undefined}
 */
CustomGameLoop.prototype.onWindowResize = function() { 
    this.fillWindow();
}
/**
 * 
 * @param {canvas} canvas
 * @returns {undefined}
 */
CustomGameLoop.prototype.initialize = function (canvas) {
    GameLoop.prototype.initialize.call(this, canvas);
this.devicePixelRatio = window.devicePixelRatio;

    var _this = this;
    window.addEventListener('resize',
                           function() {
                               _this.onWindowResize();
                            }, false);
                   var g = this.canvas.getContext("2d");

    g.scale(this.devicePixelRatio,this.devicePixelRatio);
}
/**
 * 
 * @param {int} w
 * @param {int} h
 * @returns {undefined}
 */
CustomGameLoop.prototype.setCanvasSize = function (w, h) {
    this.canvas.height = h;
    this.canvas.width = w;
}
/**
 * 
 * @param {Visual} element
 * @returns {undefined}
 */
CustomGameLoop.prototype.addElement = function (element) {
    this.elementManager.add(element);

}
/**
 * fillWindow according to the device specs and innersize
 * @returns {undefined}
 */
CustomGameLoop.prototype.fillWindow = function() {
    this.setWidth(window.innerWidth * this.devicePixelRatio) ;
    this.setHeight(window.innerHeight * this.devicePixelRatio);
}
/**
 * 
 * @param {int} width
 * @returns {undefined}
 */
CustomGameLoop.prototype.setWidth = function(width) {
    this.canvas.width = width;
     this.canvas.style.width = width/this.devicePixelRatio
}
/**
 * 
 * @returns {int} canvas width
 */
CustomGameLoop.prototype.getWidth = function() {
    return this.canvas.width ;
}
/**
 * 
 * @returns {int} canvas height
 */
CustomGameLoop.prototype.getHeight = function() {
   return this.canvas.height ;
}
/**
 * 
 * @param {int} height
 * @returns {undefined}
 */
CustomGameLoop.prototype.setHeight = function(height) {
    this.canvas.height = height;
      this.canvas.style.height = height/this.devicePixelRatio;
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
CustomGameLoop.prototype.draw = function (g) {
      g.save();
    g.scale(this.devicePixelRatio,this.devicePixelRatio);
    g.fillStyle = "lightgray";
    g.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.elementManager.drawElements(g);
    this.pointerManager.drawPointerDebugOverlay(g);
      g.restore();
     
}
/**
 * 
 * @param {int} id
 * @param {Point} pos
 * @returns {undefined}
 */
CustomGameLoop.prototype.onPointerEnter = function (id, pos) {

    this.pointerManager.onPointerEnter(id, pos);
}
/**
 * 
 * @param {int} id
 * @param {Point} pos
 * @returns {undefined}
 */
CustomGameLoop.prototype.onPointerMove = function (id, pos) {

    this.pointerManager.onPointerMove(id, pos);
}
/**
 * 
 * @param {int} id
 * @param {Point} pos
 * @returns {undefined}
 */
CustomGameLoop.prototype.onPointerActivate = function (id, pos) {

    this.pointerManager.onPointerActivate(id, pos);
}
/**
 * 
 * @param {int} id
 * @param {Point} pos
 * @returns {undefined}
 */
CustomGameLoop.prototype.onPointerDeactivate = function (id, pos) {

    this.pointerManager.onPointerDeactivate(id, pos);
}
/**
 * 
 * @param {int} id
 * @param {Point} pos
 * @returns {undefined}
 */
CustomGameLoop.prototype.onPointerLeave = function (id, pos) {

    this.pointerManager.onPointerLeave(id, pos);
}
 