/**
 * Bubble shape representation of the data input
 * @param {Array} dataObj
 * @returns {Bubble}
 */
function Bubble(dataObj) {
    this.fillColor = "orange";
    this.strokeCol = "black";
    this.strokeThick = 5;
    this.displayText = "";
    this.displayFont = "30px Aeial";
    this.ID = "Bubble";
    this.dataRowIndex = 0;
    this.dataObj = dataObj;
    //type of objects in dataObj
    this.data = "electricity";
    this.opacity = 0.6;
    this.popUp = new PopUp();
}
/**
 * 
 * @type ClickableElements
 */
Bubble.prototype = new ClickableElements();
/**
 * initialize bubble, popup
 * @returns {undefined}
 */
Bubble.prototype.initialize = function () {
    this.radius = this.dataObj[0][this.data] / 1.1;
    this.width = this.radius;
    this.height = this.radius;
    this.scalePosition();
    this.popUp.setPosition(new Point(this.pos.getX() +
            this.radius + 10, this.pos.getY()));
}
/**
 * set string for display
 * @param {String} displayName
 * @param {Font} font
 * @param {Color} color
 * @returns {undefined}
 */
Bubble.prototype.setDisplayName = function (displayName, font, color) {
    this.displayText = displayName;
    this.font = font;
    this.textColor = color;
}
/**
 * set colors by json
 * @param {JSON} colorJSON
 * @returns {undefined}
 */
Bubble.prototype.setFillColors = function (colorJSON) {
    this.fillColorJSON = colorJSON;
}
/**
 * hit test for bubbles, based on radius
 * @param {Point} pt
 * @returns {Boolean}
 */
Bubble.prototype.hitTest = function (pt) {
    if (pt.getX() < this.pos.getX() + this.width / 2 && pt.getX()
            > this.pos.getX() - this.width / 2&& pt.getY() > 
            this.pos.getY() - this.height / 2 && pt.getY() < 
            this.pos.getY() + this.height / 2) {
        this.hit = true;
    } else {
        this.hit = false;
    }
    return this.hit;
}
/**
 * changes size according to the timeBar
 * @param {int} index
 * @returns {undefined}
 */
Bubble.prototype.changeSize = function (index) {

    if (index < this.dataObj.length && index >= 0) {
        this.dataRowIndex = index;
    }
    else {
        console.info("end of data");
    }
    this.radius = this.dataObj[this.dataRowIndex][this.data];
    this.width = this.radius / this.getFactor();
    this.height = this.radius / this.getFactor();
    this.popUp.setPosition(new Point(this.pos.getX() +
            this.radius + 10,
            this.pos.getY()));
}
/**
 * get the index of the data of the bubble 
 * @returns {Number|int}
 */
Bubble.prototype.getIndex = function () {
    return this.dataRowIndex;
}
/**
 * get the corresponding unit for the bubble
 * @returns {String}
 */
Bubble.prototype.getUnit = function () {

    if (this.data === "electricity") {
        this.unit = "KW";
    }
    else if (this.data === "water") {
        this.unit = "Gallons";
    }
    return this.unit;
}
/**
 * get division factor for bubble size
 * @returns {Number}
 */
Bubble.prototype.getFactor = function () {

    if (this.data === "electricity") {
        this.factor = 1.01;
    }
    else if (this.data === "water") {
        this.factor = 3;
    }
    return this.factor;
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
Bubble.prototype.draw = function (g) {
    this.scalePosition();
    g.globalAlpha = this.opacity;
    g.fillStyle = this.fillColorJSON[this.data];
    g.strokeStyle = this.strokeCol;
    g.lineWidth = this.strokeThick;
    this.drawBubble(g);
    g.globalAlpha = 1;
    this.drawText(g);
    this.popUp.setDisplayText(this.data + ": " +
            Math.floor(this.radius) + " " + this.getUnit());
    this.popUp.draw(g);
}
/**
 * activate popup event when clicked
 * @returns {undefined}
 */
Bubble.prototype.activateEvent = function () {

    if (this.isClickedNum === 1) {
        this.popUp.isActive = false;
        this.isClickedNum = 0;
    } else {

        this.popUp.isActive = true;
        this.isClickedNum = this.isClickedNum + 1;
    }
}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
Bubble.prototype.drawBubble = function (g) {
    g.beginPath();
    TOOLS.drawEllipse(g, this.pos.getX() - this.width / 2,
    this.pos.getY() - this.width / 2, this.width, this.height);
    g.closePath();
    g.fill();
    g.stroke();
}
/**
 * draw Label for bubble
 * @param {Graphic} g
 * @returns {undefined}
 */
Bubble.prototype.drawText = function (g) {
    g.fillStyle = this.textColor;
    g.font = this.font;
    g.fillText(this.displayText, this.pos.getX(), 
    this.pos.getY() - 10);
    g.font = "15px Aeial";
}

