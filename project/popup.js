/**
 * popup class for each of the bubbles
 * @returns {PopUp}
 */
function PopUp() {
    this.showWidth = 100;
    this.showHeight = 100;
    this.fillColor = "white";
    this.strokeCol = "black";
    this.strokeThick = 5;
    this.displayText = "";
    this.showFont = "10px Aeial";
    this.ID = "PopUp";
}

PopUp.prototype = new Visual();
/**
 * 
 * @param {String} displayText
 * @returns {undefined}
 */
PopUp.prototype.setDisplayText = function (displayText) {
    this.displayText = displayText;
};
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
PopUp.prototype.draw = function (g) {
// if activated, show popup else
// make radius zero
    if (!this.isActive) {
        this.width = 0;
        this.height = 0;
        this.font = "0px Aeiel";
    } else {

        this.width = this.showWidth;
        this.height = this.showWidth;
        this.font = this.showFont;
    }
    this.drawPopUp(g);
    g.font = this.font;
    g.fillStyle = "black";
    g.fillText(this.displayText, this.pos.getX()+10, this.pos.getY() + this.height / 2);

}
/**
 * 
 * @param {Graphic} g
 * @returns {undefined}
 */
PopUp.prototype.drawPopUp = function (g) {
    g.fillStyle = this.fillColor;
    g.strokeStyle = this.strokeCol;
    g.lineWidth = this.strokeThick;
    g.beginPath();
    g.rect(this.pos.getX(), this.pos.getY(), 
    this.width, this.height);
    g.closePath();
    g.fill();
    g.stroke();
}

