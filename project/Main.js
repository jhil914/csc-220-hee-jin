/**
 * 
 * global variable for scaling based on the innerwidth and height
 * and the background size
 * @type Number
 */
var origBackgroundWidth = 750;
/**
 * 
 * global variable for scaling based on the innerwidth and height
 * and the background size
 * @type Number
 */
var origBackgroundHeight = 841;
/**
 * Shows a bubble representation of the input building data
 * and size changes depending on the timebar
 * @returns {undefined}
 */
function setupTestScenario() {
    var gameLoop = new CustomGameLoop();
    gameLoop.initialize(document.getElementById("canvas"));
    gameLoop.setCanvasSize(750, 841);
    gameLoop.fillWindow();
    // set Background
    var backgroundImage = new Background();
    backgroundImage.loadUrl("sdf.png");
    backgroundImage.setPosition(new Point(0, 0));
    backgroundImage.setWidth(750);
    backgroundImage.setHeight(841);
    gameLoop.addElement(backgroundImage);
    // parse JSON 
    var campusCenterData = JSON.parse(JSON.stringify(campuscenter));
    var fordHallData = JSON.parse(JSON.stringify(fordHall));
    var kingHouseData = JSON.parse(JSON.stringify(kingHouse));
    var sRData = JSON.parse(JSON.stringify(sRHall));
    var wilderHouseData = JSON.parse(JSON.stringify(wilderHouse));
    var chapinHouseData = JSON.parse(JSON.stringify(ChapinHouse));
    var gillettHouseData = JSON.parse(JSON.stringify(GillettHouse));

    var timeBar = new TimeBar(fordHallData);
    timeBar.initialSetting("ByHour(Drag me!)", new Point(10, 700), 50, 10);
    timeBar.setPosition(new Point(10, 700));
    timeBar.scalePosition();
    timeBar.setWidth(30);
    timeBar.setHeight(30);
    gameLoop.addElement(timeBar);
    // Filter menu Buttons
    var waterFilter = new FilterButton();
    waterFilter.setPosition(new Point(10, 600));
    waterFilter.addFilter("water");
    waterFilter.setWidth(120);
    waterFilter.setHeight(30);
    waterFilter.setFillColor("white");
    waterFilter.setStrokeColor("orange");
    waterFilter.setStrokeThickness(5);
    gameLoop.addElement(waterFilter);

    var electricityFilter = new FilterButton();
    electricityFilter.setPosition(new Point(10, 550));
    electricityFilter.addFilter("electricity");
    electricityFilter.setWidth(120);
    electricityFilter.setHeight(30);
    electricityFilter.setFillColor("white");
    electricityFilter.setStrokeColor("orange");
    electricityFilter.setStrokeThickness(5);
    gameLoop.addElement(electricityFilter);
    // set Bubbles for each building
    var campusCenter = new Bubble(campusCenterData);
    campusCenter.setPosition(new Point(450, 250));
    campusCenter.initialize();
    campusCenter.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    campusCenter.setStrokeColor("black");
    campusCenter.setStrokeThickness(5);
    campusCenter.setDisplayName("Campus Center", "30px Aeial", "black");
    campusCenter.popUp.setDisplayText("This is the Campus Center");
    gameLoop.addElement(campusCenter);

    var fordHallbubble = new Bubble(fordHallData);
    fordHallbubble.setPosition(new Point(400, 500));
    fordHallbubble.initialize();
    fordHallbubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    fordHallbubble.setStrokeColor("black");
    fordHallbubble.setStrokeThickness(5);
    fordHallbubble.setDisplayName("fordHall", "30px Aeial", "black");
    fordHallbubble.popUp.setDisplayText("This is Ford Hall");
    gameLoop.addElement(fordHallbubble);

    var kingHousebubble = new Bubble(kingHouseData);
    kingHousebubble.setPosition(new Point(100, 100));
    kingHousebubble.initialize();
    kingHousebubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    kingHousebubble.setStrokeColor("black");
    kingHousebubble.setStrokeThickness(5);
    kingHousebubble.setDisplayName("King House", "30px Aeial", "black");
    kingHousebubble.popUp.setDisplayText("This is King House");
    gameLoop.addElement(kingHousebubble);

    var srHallbubble = new Bubble(sRData);
    srHallbubble.setPosition(new Point(360, 370));
    srHallbubble.initialize();
    srHallbubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    srHallbubble.setStrokeColor("black");
    srHallbubble.setStrokeThickness(5);
    srHallbubble.setDisplayName("S-R", "30px Aeial", "black");
    srHallbubble.popUp.setDisplayText("This is Sabin-Reed");
    gameLoop.addElement(srHallbubble);

    var wilderHousebubble = new Bubble(wilderHouseData);
    wilderHousebubble.setPosition(new Point(250, 150));
    wilderHousebubble.initialize();
    wilderHousebubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    wilderHousebubble.setStrokeColor("black");
    wilderHousebubble.setStrokeThickness(5);
    wilderHousebubble.setDisplayName("Wilder House", "30px Aeial", "black");
    wilderHousebubble.popUp.setDisplayText("This is Wilder House");
    gameLoop.addElement(wilderHousebubble);

    var gillettHousebubble = new Bubble(gillettHouseData);
    gillettHousebubble.setPosition(new Point(550, 200));
    gillettHousebubble.initialize();
    gillettHousebubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    gillettHousebubble.setStrokeColor("black");
    gillettHousebubble.setStrokeThickness(5);
    gillettHousebubble.setDisplayName("Gillett House", "30px Aeial", "black");
    gillettHousebubble.popUp.setDisplayText("This is Wilder House");
    gameLoop.addElement(gillettHousebubble);

    var chapinHousebubble = new Bubble(chapinHouseData);
    chapinHousebubble.setPosition(new Point(400, 300));
    chapinHousebubble.initialize();
    chapinHousebubble.setFillColors({"electricity": "#2BBDBD",
        "water": "blue"});
    chapinHousebubble.setStrokeColor("black");
    chapinHousebubble.setStrokeThickness(5);
    chapinHousebubble.setDisplayName("Chapin House", "30px Aeial", "black");
    chapinHousebubble.popUp.setDisplayText("This is Wilder House");
    gameLoop.addElement(chapinHousebubble);

}

function initialize() {
    setupTestScenario();
}

window.onload = initialize;