/* 
 * 9/23/2015
 * Hee Jin
 * status: finished bar graph section 
 * but still working on integrating line_graph
 * problmes with not able to access data variable in line_graph.js
 * problems with swtiching form line_Graph and bar_Graph with selectors
 * 
 * function BaseClass(param)
 * if (typeof parameter == 'undefined'){
 * 
 * 
 * }else{
 * this.parameter.method();
 * }
 * this.parameter = parameter;
 * 
 * function DerivedClass(param){
 * BaseClass.prototype.call(this,parameter);
 * }
 * DerivedClass.prototype = new BaseClass();
 */

function initialize() {
    // chart_num: 0 = northampoton, 1 = Amherst, 3 = EastHampton

}
function which_chart(chart_num) {
    //clear canvas to clear all event listener
    document.getElementById("canvas_div").innerHTML = '&nbsp';
    document.getElementById("canvas_div").innerHTML = '<canvas id="myCanvas";></canvas>';
    //initialization of canvas
    var c = document.getElementById("myCanvas");

    c.width = "800";
    c.height = "800";
    c.style = "border:1px solid #000000";
    var ct = c.getContext("2d");
    ct.clearRect(0, 0, c.width, c.height);
    ct.fillStyle = "black";
    ct.font = 'italic 14pt sans-serif';
    //if else attempt to integrate line_graph
    if (chart_num < data.length) {
        //show data name
        ct.fillText(data[chart_num].getName(), 10, 20);
        
        c.addEventListener("mousemove", new draw_chart(chart_num).hover, false);
    }
    else {
        ct.fillText(data[chart_num - 3].getName(), 10, 20);
           c.addEventListener("mousemove", new line_graph(chart_num).hover, false);
    }
}
window.onload = initialize;
/** 
 * 
 * @param {type} bar :bar object
 * draws the actual individual bars
 */
function draw_bar(bar) {

    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");

    ctx.beginPath();
    ctx.rect(bar.x, bar.y, bar.width, bar.height);

    ctx.closePath();

    ctx.fillStyle = bar.color;

    ctx.fill();
    ctx.stroke();

}
/**
 * 
 * @param {type} x
 * @param {type} y
 * @param {type} color
 * @param {type} height
 * @param {type} width
 * @returns makes object of bar
 */
function bar(x, y, color, height, width) {
    this.x = x;
    this.y = y;
    this.color = color;
    this.height = height;
    this.width = width;
    this.isMouseOver = false;
    this.chart_num = 4;
    this.value = 0;
    bar.prototype.getx = function () {
        return x;
    }
    bar.prototype.gety = function () {
        return y;
    }
    bar.prototype.getColor = function () {
        return color;
    }
    bar.prototype.getHeight = function () {
        return height;
    }
    bar.prototype.getWidth = function () {
        return width;
    }


}
/**
 * 
 * @param {type} chart_num
 * @returns draws chart elements based on the chart_num input
 */
function draw_chart(chart_num) {

    var scale = 5;
    var space = 50;
    var axis_x = 50;
    var axis_y = 50;
    var pad = 0;
    var x_margin = axis_x + 10;
    var y_margin = axis_y;
    var bars = []; //stores bar objects
    var lines = [];
    // draw x y axis
    draw_Scale(axis_x, axis_y, chart_num, scale, space, x_margin);
    
    for (var i = 0; i < data[chart_num].getData().length; i++) {
        var val = data[chart_num].data[i].value;
        var new_y = (get_maxY(chart_num) - val) * scale + y_margin;
        var make_bar = new bar(x_margin + pad, new_y, 'blue', val * scale, 30);

        make_bar.chart_num = chart_num;
        make_bar.value = val;
        //  show_horizontal_line(make_line);
        draw_bar(make_bar);
        bars.push(make_bar);
        // horizontal space between each elements in chart
        pad += space;
    }
    // mouse hover function
    draw_chart.prototype.hover = function (e) {
        var mouseX, mouseY;
        
        mouseX = e.pageX;
        mouseY = e.pageY;
        //for every bars in the bars array, if the mouse in inside a particular bar, change color
        for (var i = 0; i < bars.length; i++) {
            
            if (bars[i].chart_num === chart_num) {

                if (mouseX > bars[i].x && mouseX < bars[i].x + bars[i].width && mouseY > bars[i].y)
                {
                    
                    bars[i].isMouseOver = true;
                    bars[i].color = 'red';

                    draw_bar(bars[i]);
                    show_value(bars[i], true);

                }
                else
                {
                    // console.info('outside');
                    bars[i].isMouseOver = false;
                    bars[i].color = 'blue';

                    draw_bar(bars[i]);
                    show_value(bars[i], false);

                }
            }
        }

    }

}// horizontal line object for line_graph
function line(bar, x_margin, color) {
    this.bar = bar;
    this.x_margin = x_margin;
    this.start_x = bar.x + bar.width / 2;
    this.start_y = bar.y;
    this.end_x = x_margin + 5;
    this.end_y = bar.y;
    this.color = color;

}
// draws the axis
function draw_Scale(x_margin, y_margin, chart_num, scale, space, x_margin2) {
    var pad = 0;
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#333';
    ctx.font = 'italic 8pt sans-serif';
    ctx.textAlign = "center";
    ctx.beginPath();
    ctx.moveTo(x_margin, y_margin);
    ctx.lineTo(x_margin, get_maxY(chart_num) * scale + y_margin);
    ctx.lineTo(get_maxX(chart_num) * scale, get_maxY(chart_num) * scale + y_margin);
    ctx.stroke();
    var pad_y = data[chart_num].getData().length;
    // fill x axis with data labels
    for (var i = 0; i < data[chart_num].getData().length; i++) {
        ctx.fillText(data[chart_num].data[i].label, x_margin2 + 10 + pad, get_maxY(chart_num) * scale + y_margin + 20);

        pad += space;
    }
    ctx.textAlign = "right"
    ctx.textBaseline = "middle";
    var y_scale = 0;
    // fill y axis 
    for (var i = Math.floor(get_maxY(chart_num)) - 1; i >= 0; i -= 10) {
        var diff = get_maxY(chart_num) - Math.floor(get_maxY(chart_num));
        ctx.fillText(i, x_margin - 10, y_margin + diff * scale + y_scale * scale);
        y_scale += 10;
    }

}
/**
 * 
 * @param {type} chart_num
 * @returns length of the data series
 */
function get_maxX(chart_num) {
    return data[chart_num].getData().length * 10;
}
/**
 * 
 * @param {type} chart_num
 * @returns the maximum value of the dats in the dataseries
 */
function get_maxY(chart_num) {
    var max_val = 0;

    for (j = 0; j < data[chart_num].getData().length; j++) {
        if (data[chart_num].data[j].value >= max_val) {
            max_val = data[chart_num].data[j].value;
        }
    }
    return max_val;
}
/**
 * 
 * @param {type} bar
 * @param {type} isMouseOver
 * @returns shows the values over the bars when mouse is detected on the bar
 */
function show_value(bar, isMouseOver) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    if (isMouseOver) {
        ctx.fillStyle = 'black';
        ctx.fillText(bar.value, bar.x + bar.width / 2, bar.y - 10);
//console.info((get_maxY(bar.chart_num)-bar.value)*scale -50);
    } else {
        ctx.fillStyle = 'white'
        ctx.fillText(bar.value, bar.x + bar.width / 2, bar.y - 10);
    }
}
/**
 * 
 * @param {type} line
 * @returns show horizontal line function for line_graph
 */
function show_horizontal_line(line) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");

    ctx.strokeStyle = line.color;
    ctx.beginPath();
    ctx.moveTo(line.start_x, line.start_y);
    ctx.lineTo(line.end_x, line.end_y);

    ctx.stroke();

}
