/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * function BaseClass(param)
 * if (typeof parameter == 'undefined'){
 * 
 * 
 * }else{
 * this.parameter.method();
 * }
 * this.parameter = parameter;
 * 
 * function DerivedClass(param){
 * BaseClass.prototype.call(this,parameter);
 * }
 * DerivedClass.prototype = new BaseClass();
 */

function line_graph(chart_num) {

    for (var i = 0; i < data[chart_num].getData().length; i++) {
        var val = data[chart_num].data[i].value;


        var scale = 5;
        var space = 50;
        var axis_x = 50;
        var axis_y = 50;
        var pad = 0;
        var x_margin = axis_x + 10;
        var y_margin = axis_y;
        var points = [];
        var lines = [];
        var radius = 10;
        draw_Scale(axis_x, axis_y, chart_num, scale, space, x_margin);



        var new_y = (get_maxY(chart_num) - val) * scale + y_margin;
        var make_point = new value_point(x_margin + pad, new_y, 'blue', radius);
        var make_line = new line(make_point, x_margin);
        lines.push(make_line);
        //   make_bar.chart_num = chart_num;
        //  make_bar.value = val;
        //  show_horizontal_line(make_line);
        draw_bar(make_point);
        points.push(make_point);//why are all the bars the same

        // console.info(bars.length);
        pad += space;
        line_graph.prototype.hover = function (e) {
            var mouseX, mouseY;
            //barbaabrbarbarbarbrabrbrabababrbarbarbrrb
            mouseX = e.pageX;
            mouseY = e.pageY;
            //console.info(barschart_num);    //  console.info(bars[1].getx())

            for (var i = 0; i < points.length; i++) {
                //  bars[i].updateMouseState(mouseX, mouseY);
                if (points[i].chart_num === chart_num) {

                    if (mouseX > points[i].x && mouseX < points[i].x + points[i].width && mouseY > points[i].y)
                    {
                        // console.info(bars[i].chart_num);
                        //  points[i].isMouseOver = true;
                        points[i].color = 'red';
                        lines[i].color = 'black';
                        lines[i].end_x = x_margin + 5;
                        lines[i].end_y = points[i].y;
                        show_horizontal_line(lines[i]);
                        draw_bar(points[i]);
                        show_value(points[i], true);

                    }
                    else
                    {
                        // console.info('outside');
                        //  points[i].isMouseOver = false;
                        points[i].color = 'blue';
                        lines[i].color = 'white';

                        show_horizontal_line(lines[i]);
                        draw_bar(points[i]);
                        show_value(points[i], false);

                    }
                }
            }
        }


    }


}
function draw_point(value_pt, rad) {
    var centerX = value_pt.x;
    var centerY = value_pt.y;
    var radius = rad;
    var c = document.getElementById("myCanvas");
    var context = c.getContext("2d");
    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.fillStyle = value_pt.color;
    context.fill();
    context.lineWidth = 5;
    context.strokeStyle = '#003300';
    context.stroke();

}
function value_point(x, y, color, radius) {
    this.x = x;
    this.y = y;
    this.color = color;
    this.radius = radius;
}


function line(bar, x_margin, color) {
    this.bar = bar;
    this.x_margin = x_margin;
    this.start_x = bar.x + bar.width / 2;
    this.start_y = bar.y;
    this.end_x = x_margin + 5;
    this.end_y = bar.y;
    this.color = color;

}



function get_maxX(chart_num) {
    return data[chart_num].getData().length * 10;
}
function get_maxY(chart_num) {
    var max_val = 0;

    for (j = 0; j < data[chart_num].getData().length; j++) {
        if (data[chart_num].data[j].value >= max_val) {
            max_val = data[chart_num].data[j].value;
        }
    }
    return max_val;
}

function show_value(bar, isMouseOver) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    if (isMouseOver) {
        ctx.fillStyle = 'black';
        ctx.fillText(bar.value, bar.x + bar.width / 2, bar.y - 10);
//console.info((get_maxY(bar.chart_num)-bar.value)*scale -50);
    } else {
        ctx.fillStyle = 'white'
        ctx.fillText(bar.value, bar.x + bar.width / 2, bar.y - 10);
    }
}
function show_horizontal_line(line) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");

    ctx.strokeStyle = line.color;
    ctx.beginPath();
    ctx.moveTo(line.start_x, line.start_y);
    ctx.lineTo(line.end_x, line.end_y);


    ctx.stroke();


//console.info((get_maxY(bar.chart_num)-bar.value)*scale -50);

}
